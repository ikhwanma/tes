package com.example.tesapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider

class MainActivity : AppCompatActivity() {
    private lateinit var btn_mulai:Button
    private lateinit var tv_waktu:TextView
    private lateinit var et_waktu:EditText
    private lateinit var btn_berhenti:EditText
    private lateinit var btn_reset:EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn_mulai = findViewById(R.id.btn_mulai)
        tv_waktu = findViewById(R.id.tv_waktu)
        et_waktu = findViewById(R.id.et_waktu)
        //btn_berhenti = findViewById(R.id.btn_berhenti)
        //btn_reset = findViewById(R.id.btn_reset)

        val viewModel =
            ViewModelProvider(this).get(MainActivityViewModel::class.java)
        viewModel.getDetik().observe(this, Observer {
            tv_waktu.text = it.toString()
        })
        viewModel.selesai.observe(this, Observer {
            if(it){
                Toast.makeText(this, "Selesai",

                    Toast.LENGTH_SHORT).show()

            }
        })
        btn_mulai.setOnClickListener{
            if(et_waktu.text.isEmpty() ||

                et_waktu.text.toString()=="0"){

                Toast.makeText(this, "Invalid Number",

                    Toast.LENGTH_SHORT).show()
            }else {
                viewModel.waktu.value =
                    et_waktu.text.toString().toLong()
                viewModel.startTimer()
            }
        }
        /*btn_berhenti.setOnClickListener {
            viewModel.stopTimer()
        }*/
        /*btn_reset.setOnClickListener {
            viewModel.stopTimer()
            tv_waktu.text = "0"
        }*/
    }
}
